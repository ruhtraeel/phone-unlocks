﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Core;
using ShakeGestures;
using Windows.Devices.Sensors;
using System.Diagnostics;
using Windows.UI.Xaml.Media.Imaging;



// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace Gif
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        Accelerometer accelerometer;
        DispatcherTimer timer = new DispatcherTimer();
        DispatcherTimer timerReset = new DispatcherTimer();
        DispatcherTimer rotateImage = new DispatcherTimer();
        DispatcherTimer appScreenTimer = new DispatcherTimer();
        DispatcherTimer appDelayTimer = new DispatcherTimer();
        TimeSpan timeSpan = new TimeSpan(0, 0, 2);
        TimeSpan timeSpanReset = new TimeSpan(0, 0, 7);
        TimeSpan timeSpanRotate = new TimeSpan(0, 0, 0, 0, 25);
        RotateTransform rotateTransform = new RotateTransform();
        ScaleTransform scaleTransform = new ScaleTransform();
        TranslateTransform translateTransform = new TranslateTransform();
        double corkXDelta;
        double corkYDelta;
        

        int rotateFrame = 0;

        double totalShaken = 0;
        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;

            shakeImage.ManipulationMode = ManipulationModes.Rotate;

            timer.Tick += new EventHandler<object>(timer_Tick);
            timer.Interval = timeSpan;
            timer.Start();

            timerReset.Tick += new EventHandler<object>(timerReset_Tick);
            timerReset.Interval = timeSpanReset;

            rotateImage.Tick += new EventHandler<object>(rotateImage_Tick);
            rotateImage.Interval = timeSpanRotate;
            shakeImage.RenderTransformOrigin = new Point(0.5, 0.5);

            shakeImage.RenderTransform = rotateTransform;
            appScreenImage.RenderTransform = scaleTransform;
            corkscrew.RenderTransform = translateTransform;

            corkscrew.ManipulationMode = ManipulationModes.TranslateX | ManipulationModes.TranslateY;
            corkscrew.ManipulationStarted += corkscrew_ManipulationStarted;
            corkscrew.ManipulationDelta += corkscrew_ManipulationDelta;
            corkscrew.ManipulationCompleted += corkscrew_ManipulationCompleted;
            

            mediaElement.Visibility = Visibility.Collapsed;

            appScreenImage.RenderTransformOrigin = new Point(0.5, 0.5);


            accelerometer = Accelerometer.GetDefault();
            if (accelerometer != null)
            {
                uint minReportInterval = accelerometer.MinimumReportInterval;
                uint reportInterval = minReportInterval > 16 ? minReportInterval : 16;
                accelerometer.ReportInterval = reportInterval;

                accelerometer.ReadingChanged += accelerometer_ReadingChanged;
            }

        }

        private void corkscrew_ManipulationCompleted(object sender, ManipulationCompletedRoutedEventArgs e)
        {
            Point corkscrewCoord = corkscrew.TransformToVisual(grid).TransformPoint(new Point(0, 0));
            Point rectangleCoord = rectangle.TransformToVisual(grid).TransformPoint(new Point(0, 0));
            
            if (corkscrewCoord.X >= rectangleCoord.X && corkscrewCoord.X <= (rectangleCoord.X + rectangle.Width) && corkscrewCoord.Y >= rectangleCoord.Y && corkscrewCoord.Y <= (rectangleCoord.Y + rectangle.Height))
            {
                
                openButton.Visibility = Visibility.Visible;
                moveLabel.Visibility = Visibility.Collapsed;
                instructionLabel.Visibility = Visibility.Visible;
                instructionLabel.Text = "Open!";
            }
        }

        private void corkscrew_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            translateTransform.X += e.Position.X;
            translateTransform.Y += e.Position.Y;
            corkXDelta = corkXDelta += e.Position.X;
            corkYDelta = corkYDelta += e.Position.Y;
        }

        private void corkscrew_ManipulationStarted(object sender, ManipulationStartedRoutedEventArgs e)
        {
            
        }


        private void loaded(object sender, RoutedEventArgs e)
        {
        }

        void accelerometer_ReadingChanged(Accelerometer sender, AccelerometerReadingChangedEventArgs e)
        {
            Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                //Debug.WriteLine(e.Reading.AccelerationX.ToString() + "|" + e.Reading.AccelerationY.ToString() + "|" + e.Reading.AccelerationZ.ToString());
                if (e.Reading.AccelerationX > 1.8 && e.Reading.AccelerationY > 1.8)
                {
                    totalShaken = totalShaken + e.Reading.AccelerationX;

                    // Initiate shaking bottle mode
                    if (totalShaken >= 25)
                    {
                        instructionLabel.Visibility = Visibility.Collapsed;
                        moveLabel.Visibility = Visibility.Visible;
                        timerReset.Start();
                        rotateImage.Start();
                        rectangle.Visibility = Visibility.Visible;
                        corkscrew.Visibility = Visibility.Visible;
                        corkXDelta = Canvas.GetLeft(corkscrew);
                        corkYDelta = Canvas.GetTop(corkscrew);
                    } 
                    //instructionLabel.Text = totalShaken.ToString();
                }
            });
            
        }


        private void timer_Tick(object sender, object e)
        {
            totalShaken = 0;
        }

        // If user doesn't unlock phone in time
        private void timerReset_Tick(object sender, object e)
        {
            timerReset.Stop();
            rotateImage.Stop();
            instructionLabel.Text = "Shake!";
            rotateTransform.Angle = 0;
            openButton.Visibility = Visibility.Collapsed;
            corkscrew.Visibility = Visibility.Collapsed;
            rectangle.Visibility = Visibility.Collapsed;
            moveLabel.Visibility = Visibility.Collapsed;
            translateTransform.X -= corkXDelta;
            translateTransform.Y -= corkYDelta;
        }

        private void rotateImage_Tick(object sender, object e)
        {
            
            if (rotateFrame % 4 == 0)
            {
                rotateTransform.Angle = 0;
            }
            else if (rotateFrame % 8 == 1)
            {
                rotateTransform.Angle = 10;
            }
            else if (rotateFrame % 8 == 2)
            {
                rotateTransform.Angle = 20;
            }
            else if (rotateFrame % 8 == 3)
            {
                rotateTransform.Angle = 10;
            }
            else if (rotateFrame % 8 == 4)
            {
                rotateTransform.Angle = 0;
            }
            else if (rotateFrame % 8 == 5)
            {
                rotateTransform.Angle = -10;
            }
            else if (rotateFrame % 8 == 6)
            {
                rotateTransform.Angle = -20;
            }
            else if (rotateFrame % 8 == 7)
            {
                rotateTransform.Angle = -10;
            }
            rotateFrame++;
        }

        private void openButton_Click(object sender, RoutedEventArgs e)
        {
            shakeImage.Visibility = Visibility.Collapsed;
            instructionLabel.Visibility = Visibility.Collapsed;
            rectangle.Visibility = Visibility.Collapsed;
            corkscrew.Visibility = Visibility.Collapsed;
            
            timer.Stop();
            timerReset.Stop();
            rotateImage.Stop();

            mediaElement.Visibility = Visibility.Visible;
            mediaElement.Position = TimeSpan.FromMilliseconds(900);
            mediaElement.Play();
            mediaElement.MediaFailed += mediaElement_MediaFailed;
            mediaElement.Stretch = Stretch.Fill;

            appDelayTimer.Tick += new EventHandler<object>(appDelayTimer_Tick);
            appDelayTimer.Interval = new TimeSpan(0, 0, 0, 0, 500);
            appDelayTimer.Start();

            
        }

        void mediaElement_MediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            throw new FileNotFoundException();
        }

        private void appScreenTimer_Tick(object sender, object e)
        {
            if (appScreenImage.Opacity <= 1)
            {
                appScreenImage.Opacity = appScreenImage.Opacity + 0.025;
                mediaElement.Opacity = mediaElement.Opacity - 0.025;
                scaleTransform.ScaleX = scaleTransform.ScaleX + 0.2;
                scaleTransform.ScaleY = scaleTransform.ScaleY + 0.2;
            }
            else
            {
                appDelayTimer.Stop();
                appScreenTimer.Stop();
                timer.Stop();
            }
        }

        private void appDelayTimer_Tick(object sender, object e)
        {
            appScreenTimer.Tick += new EventHandler<object>(appScreenTimer_Tick);
            appScreenTimer.Interval = new TimeSpan(0, 0, 0, 0, 1);
            appScreenTimer.Start();
            Uri uri = new Uri(BaseUri, "AppScreen.png");
            BitmapImage imgSource = new BitmapImage(uri);
            appScreenImage.Source = imgSource;
            appScreenImage.Opacity = 0;
            appDelayTimer.Stop();
        }
    }
}
