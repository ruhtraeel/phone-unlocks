﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace TouchUnlock2
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        string direction = "";
        string tempPassword = "";
        string realPassword = "";
        DispatcherTimer timer = new DispatcherTimer();
        DispatcherTimer incorrectTimer = new DispatcherTimer();
        DispatcherTimer winTimer = new DispatcherTimer();
        TranslateTransform translateTransform = new TranslateTransform();
        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;

            smiley.ManipulationMode = ManipulationModes.TranslateX | ManipulationModes.TranslateY;
            smiley.ManipulationStarted += ball_ManipulationStarted;
            smiley.ManipulationDelta += ball_ManipulationDelta;
            smiley.ManipulationCompleted += ball_ManipulationCompleted;

            Explosion.RenderTransform = translateTransform;

        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        void ball_ManipulationStarted(object sender, ManipulationStartedRoutedEventArgs e)
        {
            smiley.Source = new BitmapImage(new Uri(@"ms-appx:/Excited.png", UriKind.Absolute));
        }

        void ball_ManipulationCompleted(object sender, ManipulationCompletedRoutedEventArgs e)
        {
            if (direction == "right" && ((int)smiley.GetValue(Grid.ColumnProperty) - 1) < 4)
            {
                smiley.SetValue(Grid.ColumnProperty, (int)smiley.GetValue(Grid.ColumnProperty) + 1);
                tempPassword = tempPassword + "R";
            }
            else if (direction == "left" && ((int)smiley.GetValue(Grid.ColumnProperty) - 1) >= 0) 
            {
                smiley.SetValue(Grid.ColumnProperty, (int)smiley.GetValue(Grid.ColumnProperty) - 1);
                tempPassword = tempPassword + "L";
            }
            else if (direction == "up" && ((int)smiley.GetValue(Grid.RowProperty) - 1) >= 0)
            {
                smiley.SetValue(Grid.RowProperty, (int)smiley.GetValue(Grid.RowProperty) - 1);
                tempPassword = tempPassword + "U";
            }
            else if (direction == "down" && ((int)smiley.GetValue(Grid.RowProperty) - 1) <= 7)
            {
                smiley.SetValue(Grid.RowProperty, (int)smiley.GetValue(Grid.RowProperty) + 1);
                tempPassword = tempPassword + "D";
            }
            direction = "";
            smiley.Source = new BitmapImage(new Uri(@"ms-appx:/Smile.png", UriKind.Absolute));
        }

        void ball_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            // If user swipes to the right
            if (e.Delta.Translation.X > 0 && Math.Abs(e.Delta.Translation.X) > Math.Abs(e.Delta.Translation.Y))
            {
                direction = "right";
                
            }
            // If user swipes to the left
            else if (e.Delta.Translation.X < 0 && Math.Abs(e.Delta.Translation.X) > Math.Abs(e.Delta.Translation.Y))
            {
                direction = "left";
                
            }
            // If user swipes up
            else if (e.Delta.Translation.Y < 0 && Math.Abs(e.Delta.Translation.X) < Math.Abs(e.Delta.Translation.Y))
            {
                direction = "up";
                
            }
            // If user swipes down
            else if (e.Delta.Translation.Y > 0 && Math.Abs(e.Delta.Translation.X) < Math.Abs(e.Delta.Translation.Y))
            {
                direction = "down";
                
            }

            

        }/// 

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }

        private void confirmButton_Click(object sender, RoutedEventArgs e)
        {
            realPassword = tempPassword;
            tempPassword = "";
            infoBlock.Text = "Please swipe your password";
            resetButton.Visibility = Visibility.Collapsed;
            confirmButton.Visibility = Visibility.Collapsed;
            enterButton.Visibility = Visibility.Visible;
            smiley.SetValue(Grid.RowProperty, 3);
            smiley.SetValue(Grid.ColumnProperty, 2);
        }

        private void resetButton_Click(object sender, RoutedEventArgs e)
        {
            realPassword = "";
            smiley.SetValue(Grid.RowProperty, 3);
            smiley.SetValue(Grid.ColumnProperty, 2);
        }

        private void enterButton_Click(object sender, RoutedEventArgs e)
        {
            if (tempPassword == realPassword) 
            {
                smiley.Source = new BitmapImage(new Uri(@"ms-appx:/Win.png", UriKind.Absolute));

                
                winTimer.Interval = new TimeSpan(0, 0, 0, 0, 500);
                winTimer.Tick += winTimer_Tick;
                winTimer.Start();

            }
            else
            {

                infoBlock.Text = "INCORRECT PASSWORD";
                smiley.Source = new BitmapImage(new Uri(@"ms-appx:/Lose.png", UriKind.Absolute));
                incorrectTimer.Interval = new TimeSpan(0, 0, 1);
                incorrectTimer.Tick += incorrectTimer_Tick;
                incorrectTimer.Start();
                smiley.ManipulationDelta -= ball_ManipulationDelta;
                Point smileyCoord = smiley.TransformToVisual(grid).TransformPoint(new Point(0, 0));
                translateTransform.X = smileyCoord.X - 150;
                translateTransform.Y = smileyCoord.Y - 200;
                Explosion.Visibility = Visibility.Visible;
                tempPassword = "";
            }
        }

        void winTimer_Tick(object sender, object e)
        {
            winTimer.Stop();

            Uri uri = new Uri(BaseUri, "AppScreen.png");
            BitmapImage imgSource = new BitmapImage(uri);
            appScreen.Source = imgSource;
            infoBlock.Visibility = Visibility.Collapsed;
            enterButton.Visibility = Visibility.Collapsed;
            appScreen.Opacity = 0;
            timer.Tick += new EventHandler<object>(timer_Tick);
            timer.Interval = new TimeSpan(0, 0, 0, 0, 1);
            timer.Start();
        }

        void incorrectTimer_Tick(object sender, object e)
        {
            smiley.SetValue(Grid.RowProperty, 3);
            smiley.SetValue(Grid.ColumnProperty, 2);
            smiley.Source = new BitmapImage(new Uri(@"ms-appx:/Smile.png", UriKind.Absolute));
            incorrectTimer.Stop();
            smiley.ManipulationDelta += ball_ManipulationDelta;
            infoBlock.Text = "Please swipe your password";
            Explosion.Visibility = Visibility.Collapsed;
        }

        private void timer_Tick(object sender, object e)
        {
            if (appScreen.Opacity <= 1)
            {
                appScreen.Opacity = appScreen.Opacity + 0.05;
            }
            else
            {
                timer.Stop();
            }
        }
    }
}
